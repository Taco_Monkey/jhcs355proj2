/**
 * Created by hultm on 5/17/2017.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM school_;';
    console.log(query);
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(s_id, callback) {
    var query = 'call school_info(?)';
    var queryData = [s_id];
    console.log(query);
    connection.query(query, queryData, function (err, result) {
        callback(err, result)
    });
};

exports.insert = function(params, callback) {
    var query = 'insert into school_ (name, address) values (? ,?)';
    var queryData = [params.name, params.address];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.delete = function(s_id, callback) {
    var query = 'delete from school_ where s_id = ?';
    var queryData = [s_id];
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};