/**
 * Created by hultm on 5/12/2017.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view skill_view as
 select s.*, a.street, a.zip_code from skill s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM team;';
    console.log(query);
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(team_id, callback) {
    var query = 'select team_name, name ' +
        'from team t ' +
        'left join school_ s on t.school = s.s_id ' +
        'where t.team_id = ?';
    var queryData = [team_id];
    console.log(query);
    connection.query(query, queryData, function (err, result) {
        callback(err, result);

    });
};

exports.getPersons = function(team_id, callback) {
    var query = 'select p_id, team_name, position, first_name, last_name from team t ' +
    'left join person_on_team pot on pot.team_id = t.team_id ' +
    'where t.team_id = ?';
    var queryData = [team_id];
    console.log(query);
    connection.query(query, queryData, function (err, result) {
        callback(err, result)
    });
};

exports.getPersonInfo = function(p_id, callback) {
    var query = 'call player_info(?)';
    var queryData = [p_id];
    console.log(query);
    connection.query(query, queryData, function (err, result) {
        callback(err, result)
    });
};

exports.insert = function(params, callback) {
    var query = 'insert into team (team_name, school) values (? ,?)';
    var queryData = [params.team_name, params.school];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.delete = function(team_id, callback) {
    var query = 'delete from team where team_id = ?';
    var queryData = [team_id];
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};


exports.insertPerson = function(params, callback) {
    var query = 'INSERT INTO person_on_team (first_name, last_name, position, address, team_id) VALUES (?, ?, ?, ?, ?)';
    var queryData = [params.first_name, params.last_name, params.position, params.address, params.team_id];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.insertStudent = function(params, callback) {
    var query = 'INSERT INTO student_ (p_id, gpa, school_id, school) VALUES (?, ?, ?, ?)';
    var queryData = [params.p_id, params.gpa, params.school_id, params. school];
    console.log(query);
    console.log(queryData);
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};