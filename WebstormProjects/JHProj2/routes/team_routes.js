/**
 * Created by hultm on 5/12/2017.
 */
var express = require('express');
var router = express.Router();
var team_dal = require('../model/team_dal');
var school_dal = require('../model/school_dal');
var address_dal = require('../model/address_dal');

// View All team
router.get('/all', function(req, res) {
    team_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('team/teamViewAll', { 'result':result });
        }
    });

});

// View Team by id
router.get('/', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.getById(req.query.team_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('team/teamViewById', {'result': result});
            }
        });
    }
});

// View Players for a team
router.get('/players', function (req, res) {
    if (req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.getPersons(req.query.team_id, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('team/playerView', {'result': result});
            }
        });
    }
});

// View player by id
router.get('/players/info', function (req, res) {
    if (req.query.p_id == null) {
        res.send('p_id is null');
    }
    else {
        team_dal.getPersonInfo(req.query.p_id, function (err, result) {
            if(err) {
                res.send(err);
            }
            else {
                res.render('team/playerById', {'result': result});
            }
        });
    }
});

// Add Team
router.get('/add', function (req, res) {
    school_dal.getAll((function (err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('team/teamAdd', {'schools': result});
        }
    }));
});

// Insert Team
router.get('/insert', function(req, res) {
    if(req.query.team_name == null) {
        res.send('Team Name must be provided.');
    }
    else if(req.query.school == null) {
        res.send('School not selected');
    }
    else {
        team_dal.insert(req.query, function(err, result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                res.redirect(302, '/team/all');
            }
        });
    }
});

// Add a player to a team
router.get('/playerAdd', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    address_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            team_dal.getAll(function (err, teams) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('team/playerAdd', {'result': result, 'teams': teams});
                }
            });

        }
    });
});

// Insert Person
router.get('/person/insert', function(req, res) {
    if(req.query.first_name == null) {
        res.send('First Name must be provided.');
    }
    else if(req.query.last_name == null) {
        res.send('Last Name not provided');
    }
    else if(req.query.position == null) {
        res.send('position not provided');
    }
    else if(req.query.team_id == null) {
        res.send('Team not selected');
    }
    else {
        team_dal.insertPerson(req.query, function(err, result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                res.redirect(302, '/team/all');
            }
        });
    }
});

// Make student
router.get('/student/', function(req, res) {
    school_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            team_dal.getPersonInfo(req.query.p_id, function (err, person) {
                if(err) {
                    res.send(err);
                }
                else {
                    res.render('team/student', {'person': person, 'result': result});
                }
            });

        }
    });
});

router.get('/student/insert/', function(req, res) {
    if(req.query.gpa == null) {
        res.send('GPA must be provided.');
    }
    else if(req.query.school == null) {
        res.send('School not selected');
    }
    else {
        team_dal.insertStudent(req.query, function(err, result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                res.redirect(302, '/team/all');
            }
        });
    }
});

router.get('/delete', function(req, res) {
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.delete(req.query.team_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/team/all');
            }
        });
    }
});

module.exports = router;














